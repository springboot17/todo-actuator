package com.apress.todo.actuator;

import io.micrometer.core.instrument.MeterRegistry;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.web.servlet.HandlerInterceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.util.Locale;

@Log4j2
@RequiredArgsConstructor
public class ToDoMetricInterceptor implements HandlerInterceptor {
    private final MeterRegistry meterRegistry;
    private String URI, pathKey, METHOD;

    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) throws Exception {
        URI = request.getRequestURI();
        METHOD = request.getMethod();
        if (!URI.contains("prometeus")) {
            log.info(" >> PATH: {}", URI);
            log.info(" >> METHOD: {}", METHOD);
            pathKey = "api_".concat(METHOD.toLowerCase())
                    .concat(URI.replace("/", "_").toLowerCase(Locale.ROOT));
            this.meterRegistry.counter(pathKey).increment();
        }
    }
}
