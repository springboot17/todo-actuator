package com.apress.todo.validator;

import org.springframework.validation.Errors;
import org.springframework.validation.ObjectError;

/**
 * Class ToDoValidationErrorBuilder
 * фабрика валидатора
 *
 * @author Kaleganov Alexander
 * @since 26 дек. 20
 */
public class ToDoValidationErrorBuilder {


    public static ToDoValidationError fromBindingErrors(Errors errors) {
        ToDoValidationError error = new ToDoValidationError("Validation failed. " + errors.getErrorCount() + " error(s)");
        for (ObjectError objectError : errors.getAllErrors()) {
            error.addValidationError(objectError.getDefaultMessage());
        }
        return error;
    }
}
