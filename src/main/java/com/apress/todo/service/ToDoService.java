package com.apress.todo.service;

import java.util.Collection;
import java.util.List;

/**
 * Class ToDoService
 * todo: описание
 *
 * @author Kaleganov Alexander
 * @since 05 янв. 21
 */
public interface ToDoService<T> {
    public T save(T domain);
    public List<T> save(Collection<T> domains);
    public void delete(Long id);
    public T findById(Long id);
    public List<T> findAll();
}
