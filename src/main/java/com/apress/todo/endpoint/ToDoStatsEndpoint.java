package com.apress.todo.endpoint;

import org.springframework.boot.actuate.endpoint.annotation.DeleteOperation;
import org.springframework.boot.actuate.endpoint.annotation.Endpoint;
import org.springframework.boot.actuate.endpoint.annotation.ReadOperation;
import org.springframework.boot.actuate.endpoint.annotation.Selector;
import org.springframework.boot.actuate.endpoint.annotation.WriteOperation;
import org.springframework.stereotype.Component;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.RequiredArgsConstructor;

import com.apress.todo.domain.ToDo;
import com.apress.todo.repository.ToDoRepository;

@Component
@RequiredArgsConstructor
@Endpoint(id="todo-stats")
public class ToDoStatsEndpoint {

    private final ToDoRepository toDoRepository;

    @ReadOperation
    public Stats stats() {
        return new Stats(this.toDoRepository.count(), this.toDoRepository.countByCompleted(true));
    }

    @ReadOperation
    public ToDo getToDo(@Selector Long id) {
        return this.toDoRepository.findById(id).orElse(null);
    }

    @WriteOperation
    public Operation completeToDo(@Selector Long id) {
        ToDo toDo = this.toDoRepository.findById(id).orElse(null);
        if(null != toDo){
            toDo.setCompleted(true);
            this.toDoRepository.save(toDo);
            return new Operation("COMPLETED", true);
        } else {
            this.toDoRepository.save(new ToDo(id));
        }

        return new Operation("COMPLETED", false);
    }

    @DeleteOperation
    public Operation removeToDo(@Selector Long id) {
        try {
            this.toDoRepository.deleteById(id);
            return new Operation("DELETED", true);
        }catch(Exception ex){
            return new Operation("DELETED", false);
        }
    }

    @AllArgsConstructor
    @Data
    public static class Stats {
        private long count;
        private long completed;
    }

    @AllArgsConstructor
    @Data
    public static class Operation{
        private String name;
        private boolean successful;
    }
}
