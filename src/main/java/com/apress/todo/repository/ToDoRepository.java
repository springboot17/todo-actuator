package com.apress.todo.repository;

import com.apress.todo.domain.ToDo;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import java.util.Collection;

/**
 * Class CommonRepository
 * todo: репозиторий
 *
 * @author Kaleganov Alexander
 * @since 26 дек. 20
 */
@Repository
public interface ToDoRepository extends JpaRepository<ToDo, Long>, JpaSpecificationExecutor<ToDo> {
     long countByCompleted(boolean completed);

}
